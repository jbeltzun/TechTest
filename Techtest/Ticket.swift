//
//  Ticket.swift
//  Techtest
//
//  Created by Julien Beltzung on 02/10/2017.
//  Copyright © 2017 Julien Beltzung. All rights reserved.
//

import Foundation

enum SerializationError: Error {
    case missing(String)
}

struct Ticket: Equatable {
    
    let id: String
    var type: String = ""
    var subject: String?
    var description: String?
    var priority: String?
    var status: String?

}

extension Ticket {
    
    init(json: [String: Any]) throws {
        
        guard let id = json["id"] else {
            throw SerializationError.missing("id")
        }
        
        guard let subject = json["subject"] as? String else {
            throw SerializationError.missing("subject")
        }
        
        guard let description = json["description"] as? String else {
            throw SerializationError.missing("description")
        }
        
        guard let priority = json["priority"] as? String else {
            throw SerializationError.missing("priority")
        }
        
        guard let status = json["status"] as? String else {
            throw SerializationError.missing("status")
        }
        
        if let type = json["type"] as? String {
            self.type = type
        }
        
        self.id = "\(id)"
        self.subject = subject
        self.description = description
        self.priority = priority
        self.status = status
        
    }
}

func ==(lhs: Ticket, rhs: Ticket)-> Bool {
    return lhs.id == rhs.id
}

