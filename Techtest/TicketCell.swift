//
//  TicketCell.swift
//  Techtest
//
//  Created by Julien Beltzung on 02/10/2017.
//  Copyright © 2017 Julien Beltzung. All rights reserved.
//

import Foundation
import UIKit

class TicketCell: UITableViewCell {
    static let reuseIdentifier = "TicketCell"

    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var typeIcon: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    
    
    func configureWithTicket(ticket: Ticket) {
        self.subjectLabel.text = ticket.subject
        self.descriptionLabel.text = ticket.description
        self.statusLabel.text = ticket.status?.capitalized
        self.idLabel.text = "#" + ticket.id
        self.typeIcon.layer.cornerRadius = 10;
        switch ticket.type {
        case "problem":
            self.typeIcon.backgroundColor = UIColor.black
        case "indident":
            self.typeIcon.backgroundColor = UIColor.red
        case "question":
            self.typeIcon.backgroundColor = UIColor.blue
        case "task":
            self.typeIcon.backgroundColor = UIColor.yellow
        default: 
            self.typeIcon.backgroundColor = UIColor.white
        }
        
    }
    
}
