//
//  Extensions.swift
//  Techtest
//
//  Created by Julien Beltzung on 02/10/2017.
//  Copyright © 2017 Julien Beltzung. All rights reserved.
//

import Foundation

extension URLRequest {
    
    static func zendeskRequestWithSubdomain(_ subdomain:String, identifier:String) -> URLRequest {
        
        let zUrl = URL(string:("https://\(subdomain).zendesk.com/api/v2/views/\(identifier)/tickets.json"))
        
        var request = URLRequest(url: zUrl!)
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")

        let email = "acooke+techtest@zendesk.com"
        let password = "mobile"
        let loginString = String(format: "%@:%@", email, password)
        let loginData = loginString.data(using: String.Encoding.utf8)!
        let base64LoginString = loginData.base64EncodedString()
        
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")

        return request
    }
    
}


extension Data {
    
    func parseTicketList() -> [Ticket] {
        
        var tickets = [Ticket]()
        do {
            let jsonResult = try JSONSerialization.jsonObject(with: self)
            if let json = jsonResult as? [String:Any] {
                if let list = json["tickets"] as? [[String:Any]] {
                    for entry in list  {
                        do {
                            let ticket = try Ticket(json: entry)
                            tickets.append(ticket)
                        } catch {
                            print(error.localizedDescription)
                        }
                    }
                }
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        return tickets
    }
}
