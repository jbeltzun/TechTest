//
//  ViewController.swift
//  Techtest
//
//  Created by Julien Beltzung on 02/10/2017.
//  Copyright © 2017 Julien Beltzung. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var tickets : [Ticket] = [Ticket]()

    @IBOutlet weak var ticketTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.alpha = 0.0
        
        let request = URLRequest.zendeskRequestWithSubdomain("mxtechtest", identifier:"39551161")
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let data = data {
                self.tickets = data.parseTicketList() as [Ticket]
                
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.tableView.alpha = 1.0
                    })
                    self.ticketTitle.text = "Tickets : \(self.tickets.count)"
                    self.tableView.reloadData()
                }
            } else {
                print("Something went wrong \(error!)")
            }
        }.resume()
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tickets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: TicketCell.reuseIdentifier, for: indexPath) as! TicketCell
        cell.configureWithTicket(ticket: tickets[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

